package stopwatch;

/**
 * Main application.
 * @author Pipatpol Tanavongchinda
 * @version 2015-01-29
 */
public class Main {
	/**
	 * Run the tasks.
	 */
	public static void main(String[] args)
	{
		Runnable[] tasks = { new Task1(100000), 
							 new Task2(100000), 
							 new Task3(100000000), 
							 new Task4(100000000), 
							 new Task5(100000000)};
		
		TaskTimer taskTimer = new TaskTimer();
		
		for(Runnable task : tasks)
		{
			taskTimer.measureAndPrint(task);
		}
	}
}
