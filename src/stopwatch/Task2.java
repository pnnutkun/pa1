package stopwatch;

/**
 * Append 100,000 characters to a StringBuilder, then convert result to String and display its length.
 * @author Pipatpol	Tanvavongchinda
 * @version 2015-01-29
 */
public class Task2 implements Runnable{
	/** The loop counter used in the tasks. */
	private int counter;
	/** A char that add to String. */
	final char CHAR = 'a';
	
	/**
	 * Initialize Task2.
	 * @param counter is the loop counter.
	 */
	public Task2(int counter)
	{
		this.counter = counter;
	}
	
	/**
	 * Add a character counter times.
	 */
	public void run()
	{
		StringBuilder builder = new StringBuilder(); 
		int k = 0;
		while(k++ < counter) {
			builder = builder.append(CHAR);
		}
		// now create a String from the result, to be compatible with task 1.
		String result = builder.toString();
		System.out.println("final string length = " + result.length());
	}
	
	/**
	 * Describe this task what is it doing.
	 * @return Tell what does this task do.
	 */
	public String toString()
	{
		return String.format("Append to StringBuilder with count=%,d\n", counter);
	}
}
