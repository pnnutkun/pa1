package stopwatch;

import java.math.BigDecimal;

/**
 * Sum 100,000,000 BigDecimal objects.
 * @author Pipatpol	Tanvavongchinda
 * @version 2015-01-29
 */
public class Task5 implements Runnable{
	/** The loop counter used in the tasks. */
	private int counter;
	/** The size of array. */
	static final int ARRAY_SIZE = 500000;
	/** Create array of BigDecimal. */
	BigDecimal[] values = new BigDecimal[ARRAY_SIZE];
	
	/**
	 * Initialize Task5.
	 * @param counter is the loop counter.
	 */
	public Task5(int counter)
	{
		this.counter = counter;
		for(int i=0; i<ARRAY_SIZE; i++) values[i] = new BigDecimal(i+1);
	}
	
	/**
	 * Summation of this array.
	 */
	public void run()
	{
		BigDecimal sum = new BigDecimal(0.0);
		for(int count=0, i=0; count<counter; count++, i++) {
			if (i >= ARRAY_SIZE) i = 0;
			sum = sum.add( values[i] );
		}
		System.out.println("sum = " + sum);
	}
	
	/**
	 * Describe this task what is it doing.
	 * @return Tell what does this task do.
	 */
	public String toString()
	{
		return String.format("Sum array of BigDecimal with count=%,d\n", counter);
	}
}
