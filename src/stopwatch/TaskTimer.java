package stopwatch;

/**
 * A TaskTimer that make a code in SpeedTest.java
 * easily to read.
 * @author Pipatpol Tanavongchinda
 * @version 2015-01-29
 */
public class TaskTimer{
	
	/**
	 * Create a stopwatch and record time that task does.
	 * @param runnable is a task can be Task1, Task2, Task3, Task4 and Task5.
	 */
	public void measureAndPrint( Runnable runnable )
	{
		StopWatch timer = new StopWatch();

		timer.start();
		System.out.print(runnable.toString());
		runnable.run();
		timer.stop();
		System.out.printf("Elapsed time %.6f sec\n\n", timer.getElapsed());

	}
}
