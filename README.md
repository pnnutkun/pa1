# Stopwatch by Pipatpol Tanavongchinda (5710546372)

I ran the tasks on ASUS K55VJ, Windows 8.1 Pro, and got these
results:

Task| Time
--------------------------------------|-------------:
Append 100,000 chars to String        | 2.765983 sec
Append 100,000 chars to StringBuilder | 0.003189 sec
Sum array size 100,000,000 of double primitives | 0.099553 sec
Sum array size 100,000,000 of Double objects | 0.558728 sec
Sum array size 100,000,000 of BigDecimal | 0.931367 sec


## Explanation of Results

Because of CPU Clock rate, amount of char and large array
make the time different.